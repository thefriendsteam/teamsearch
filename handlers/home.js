module.exports = function(req, res, next){
    res.render('index', {
        title: 'Team Search',
        user: req.user
    });
};

module.exports.profile = function (req, res, next) {
    res.render('profile',{
        title: 'Team Search : Profile',
        user: req.user
    });
};

module.exports.about = function(req, res, next){
    res.render('about',{
        title: 'Team Search : About',
        user: req.user
    });
};