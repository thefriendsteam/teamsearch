var mongoose = require('./../libs/mongoose');

module.exports.login = function (req, res, next) {
    res.render('auth/login', {
        message: req.flash('loginMessage'),
        user: req.user,
        title: 'Team Search : Login'
    });
};

module.exports.signup = function(req, res, next){
    res.render('auth/signup', {
        message: req.flash('signupMessage'),
        user: req.user,
        title: 'Team Search : Sign up'
    });
};

module.exports.logout = function (req, res, next) {
    req.logout();
    res.redirect('/');
};



module.exports.isLoggedIn = function(req, res, next) {
    if (req.isAuthenticated()){
        if (req.user.activated) {
            return next();
        } else {
            if (req.url.toLowerCase() === '/auth/activate') {
                return next();
            } else {
                res.redirect('/auth/activate');
            }
        }
    } else {
        res.redirect('/auth/login');
    }

};

module.exports.isNotLogIn = function (req, res, next) {
    if (req.isAuthenticated()) {
        res.redirect('/');
    } else {
        return next();
    }
};



module.exports.activate = function (req, res) {
    mongoose.models('activateMessage').activateUser(req.params.id, function (err) {
        if (err) {
            res.end(err.toString());
        } else {
            res.redirect('/profile');
        }

    });
};

module.exports.resendActivateMsg = function (req, res) {
    mongoose.models('activateMessage').resend(req.user._id, function (err) {
        if (err) {
            res.end(err.toString());
        } else {
            res.redirect('/auth/activate');
        }
    })
};

module.exports.notActive = function (req, res) {
    mongoose.models('activateMessage').newMsg(req.user, function (err) {
        if (err) {
            res.end(err.toString());
        } else {
            res.render('auth/activateUser', {
                user: req.user
            });
        }
    });
};