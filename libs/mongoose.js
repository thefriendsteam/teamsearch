var config = require('./config');
    mongoose = require('mongoose');
    fs = require('fs');
    path = require('path');
    async = require('async');

mongoose.Promise = global.Promise;
mongoose.connect(config.get('mongoose:url'), config.get('mongoose:options'));

var db = mongoose.connection;

db.on('error', function (err) {
    console.log('error ' + err.toString());
    mongoose.close();
})

var models = {};

module.exports.init = function (modelsDirectory) {
    var schemaList = fs.readdirSync(modelsDirectory);

    async.eachSeries(schemaList, function (item, cb) {
        if(path.extname(item) === '.js') {
            var modelName = path.basename(item, '.js');
            models[modelName.toLocaleLowerCase()] = require(path.join(modelsDirectory, modelName))(mongoose);
        }
        cb();
    });
};


module.exports.models = function (modelName) {
    var name = modelName.toLowerCase();
    if (typeof models[name] == 'undefined') {
        throw 'Model "' + name + '" is not exist';
    }
    return models[name];
};

