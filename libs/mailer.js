var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');

var mailer = require('nodemailer');
var config = require('./config');

var smtpConfig = {
    host: config.get('postfixHost'),
    port: config.get('postfixPort'),
};

var transporter = mailer.createTransport(smtpConfig);

module.exports.sendEmailActivate = function (user, msg, callback) {
    var mailParam = {name: user.displayName, link: msg.link};

    var templateDir = path.join(path.parse(__dirname).dir, 'views', 'mail_templates', 'activate');

    var activateTamplate = new EmailTemplate(templateDir);

    activateTamplate.render(mailParam, function (err, result) {
        if (err) {
            callback(err.toString());
        } else {
            var mailOptions = {
                from: '"Team Search" <no-reply@teamsearch.pro>',
                to: user.email,
                subject: 'Activate user',
                html: result.html
            };

            transporter.sendMail(mailOptions, function (err, info) {
                if (err) {
                    callback(err.toString());
                } else {
                    callback(null, info.toString());
                }
            })
        }
    })

};