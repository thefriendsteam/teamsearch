var mongoose = require('mongoose');
var db = require('./mongoose');

module.exports = function(modelName){

    var list = function(req, res){
        db.models(modelName).find(function(err, entities){
            if(err){
                res.send(err)
            } else res.json(entities);
        });
    };

    var get = function(req, res){
        var id = req.params.id;
        db.models(modelName).findById(id, function(err, entity){
            if(err){
                res.send(err);
            } else res.json(entity);
        })
    };

    var create = function (req, res) {
        var entity = req.body;
        db.models(modelName).create(entity, function(err){
            if(err){
                res.send(err);
            } else res.json({msg: modelName + ' created!'});
        });
    };

    var update = function(req, res){
        var data = req.body;
        var id = req.params.id;

        db.models(modelName).findOneAndUpdate({_id: id}, data,function(err, entity){
            if(err){
                res.send(err);
            } else res.json(entity);
        });
    };

    var remove = function(req, res){
        var id = req.params.id;
        db.models(modelName).findOneAndRemove({_id: id}, function(err){
            if(err){
                res.send(err);
            } else res.json(modelName + ' removed!');
        });
    };

    return {
        list: list,
        get: get,
        create: create,
        update: update,
        remove: remove
    }
};