var express       = require('express');
var session       = require('express-session');

var path          = require('path');
var logger        = require('morgan');

var cookieParser  = require('cookie-parser');
var bodyParser    = require('body-parser');

var passport      = require('passport');
var flash         = require('connect-flash');

/** OUR MODULES */
var routes = require('./routes');
var config = require('./libs/config');
var db = require('./libs/mongoose');

/** SETUP DATABASE */
db.init(path.join(__dirname, 'models'));

require('./libs/passport')(passport);

/** EXPRESS */
var app = express();

/** VIEW ENGINE SETUP */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

/** SETUP COMPILER LESS */
app.use(require('less-middleware')(path.join(__dirname, 'public')));
/** SETUP FRONT-END'S FILES */
app.use(express.static(path.join(__dirname, 'public')));

/** SETUP PASSPORT */
app.use(session({
  secret: 'ilovetsteamsearchproorgcomwearesupermansteams',
  resave: true,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

/** SETUP ROUTERS */
var handlers = {
  auth: require('./handlers/auth'),
  home: require('./handlers/home'),
  users: require('./handlers/users')
};
//associate handlers with router
app.use('/', routes.setup(handlers, passport));

/** CATCH 404 AND FORWARD TO ERROR HANDLER */
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/** ERROR HANDLERS */

// DEVELOPMENT ERROR HANDLER
// WILL PRINT STACKTRACE
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err,
      user: req.user
    });
  });
}

// PRODUCTION ERROR HANDLER
// NO STACKTRACES LEAKED TO USER
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {},
    user: req.user
  });
});


module.exports = app;
