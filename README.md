# TeamSearch

It is great project about how to find a good teammate. 

Out site: http://teamsearch.pro

Team chat : https://teamsearch-group.slack.com/home

## TODO List
- **Account Management** 
    - Avatar
    - Profile Details
    - Change Password
    - Forgot Password
    - Reset Password
    - Delete Account 

## Dependency
- [MongoDB 3.2.10+](https://www.mongodb.org/downloads)
- [Node.js 7.0+](http://nodejs.org/)
- [Express 4.13.4+](http://expressjs.com/ru/starter/installing.html)

**NOTE:** If you are new to Node or Express, I recommend to watch 

- [Node.js](https://learn.javascript.ru/screencast/nodejs)
- [Express](http://expressjs.com/ru/)

That can help you.

## Getting Started
- Clone the repository
```bash
#Get the last snapshot
git clone git@bitbucket.org:thefriendsteam/teamsearch.git
```
- Install NPM dependencies
```bash
npm install
```
- Run application
```bash
npm start
```
or you can use also
```bash
node ./bin/www
```

## Project Structure
|Name |Description |
|-----|------------|

## License
TODO: Write license