var path = require('path');

module.exports = function (mongoose) {
    var schema = new mongoose.Schema({
        teamId: {type: mongoose.Schema.ObjectId, required: true},
        projectId: {type: mongoose.Schema.ObjectId, default: null},
        description: {type: String, default: ""},
        author: {type: mongoose.Schema.ObjectId, required:true},
        isPrivate: {type: Boolean, required: true, default: false}

    });

    return mongoose.model(path.basename(module.filename, '.js'), schema);
};

