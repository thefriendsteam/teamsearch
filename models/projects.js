var path = require('path');

module.exports = function (mongoose) {
    var schema = new mongoose.Schema({
        teamId: {type: mongoose.Schema.ObjectId, unique: true, required: true},
        name: {type: String, unique: true, required: true},
        avatarLink: {type: String, default: ""},
        descriptionPublic: {type: String, default: ""},
        descriptionPrivate: {type: String, default: ""},
        isPrivate: {type: Boolean, default: false},
        gitLink: {type: String, default: ""}
    });

    return mongoose.model(path.basename(module.filename, '.js'), schema);
};
