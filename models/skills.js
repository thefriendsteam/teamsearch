var path = require('path');

module.exports = function (mongoose) {
    var schema = new mongoose.Schema({
        parentId: {type: mongoose.Schema.ObjectId, default: null},
        isGroup: {type: Boolean, required: true, default: false},
        name: {type: String, required: true},
        description: {type: String, default: ""}
    });

    return mongoose.model(path.basename(module.filename, '.js'), schema);
};

