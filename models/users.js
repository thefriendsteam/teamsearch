var crypto = require('crypto');
var async = require('async');
var path = require('path');

module.exports = function (mongoose) {
    var schema = new mongoose.Schema({
        email: {type: String, unique: true, required: true},
        hashedPassword: {type: String, required: true},
        salt: {type: String, required: true},
        created: {type: Date, default: Date.now},
        firstName: {type: String},
        lastName: {type: String},
        displayName: {type: String, required: true, unique: true},
        avatarLink: {type: String, default: ""},
        number: {type: String, default: ""},
        description: {type: String, default: ""},
        dateOfBirth: {type: Date, default: "01.01.1970"},
        activated: {type: Boolean, default: false},
        skills: [{skillId: mongoose.Schema.Types.ObjectId, proficiency: String}],
        teams: [{teamId: mongoose.Schema.Types.ObjectId, position: String}],
        speakSkills: [{speakId: mongoose.Schema.Types.ObjectId, proficiency: String}]
    });

    schema.virtual('password')
        .set(function (password) {
            this._plainPassword = password;
            this.salt = Math.random() + '';
            this.hashedPassword = this.encryptPassword(password);
        })
        .get(function () {
            return this._plainPassword;
        });


    schema.methods.encryptPassword = function (password) {
        return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
    };

    schema.methods.checkPassword = function (password) {
        return this.encryptPassword(password) === this.hashedPassword;
    };

    schema.statics.registration = function (regInfo, callback) {

        var User = this;

        async.waterfall([
            function (callback) {
                User.findOne({email: regInfo.email}, callback);
            },
            function (user, callback) {
                if (user) {
                    callback('This email is already in use');
                } else {
                    let user = new User(regInfo);

                    user.save(function (err) {
                        if (err) {
                            callback(err);
                        } else callback(null, user);
                    });
                }
            }
        ], callback);

    };

    schema.static.authorization = function (authInfo, callback) {
        var User = this;

        async.waterfall([
            function (callback) {
                User.findOne({email: authInfo.email}, callback);
            },
            function (user, callback) {
                if (user) {
                    if (user.checkPassword(authInfo.password)) {
                        callback(null, user);
                    } else {
                        callback('Incorrect username and/or password!')
                    }
                } else {
                    callback('Incorrect username and/or password!');
                }
            }
        ], callback);
    };

    return mongoose.model(path.basename(module.filename, '.js'), schema);
};
