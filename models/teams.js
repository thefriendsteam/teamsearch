var path = require('path');

module.exports = function (mongoose) {
    var schema = new mongoose.Schema({
        name: {type: String, unique: true, required: true},
        avatarLink: {type: String, default: ""},
        description: {type: String, default: ""},
        isPrivate: {type: Boolean, default: false},
        teamLeader: {type: mongoose.Schema.ObjectId, required: true},
        author: {type: mongoose.Schema.ObjectId, required: true},
        mainLanguage: {type: mongoose.Schema.ObjectId, required: true},

    });

    return mongoose.model(path.basename(module.filename, '.js'), schema);
};

