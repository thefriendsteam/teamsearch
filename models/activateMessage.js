var path = require('path');
var mailer = require('./../libs/mailer');
var async =  require('async');
var config = require('nconf');
var mongo = require('./../libs/mongoose');

module.exports = function (mongoose) {
    var schema = new mongoose.Schema({
        userId: {type: mongoose.Schema.ObjectId, required: true, unique: true},
        email: {type: String, required: true},
        link: {type: String, required: true}
    });

    schema.statics.newMsg = function (user, callback) {
        var Message = this;

        async.waterfall([
            function (callback) {
                Message.findOne({userId: user._id}, callback);
            },
            function (msg) {
                if(msg)
                {
                    callback(null, msg);
                } else {

                    var msg = new Message({userId: user._id, email: user.email, link: generationActivateLink(user._id)});

                    msg.save(function (err) {
                        if (err){
                            callback(err.toString());
                        }else {
                            Message.sendActivatedLink(user, msg, callback);
                        }
                    });
                }
            }
        ],callback);

    };

    schema.statics.activateUser = function (userId, callback) {

        async.waterfall([
            function (callback) {
                mongo.models('Users').findById(userId, callback);
            },
            function (user) {
                if (user) {
                    user.activated = true;
                    user.save(callback);
                } else {
                    callback('User not found');
                }
            }
        ], callback);

    };

    schema.statics.resend = function (userId, callback) {
        var Message = this;

        async.waterfall([
            function (callback) {
                Message.findOne({userId: userId}, callback);
            },
            function (msg, callback) {
                if (msg){
                    mongo.models('Users').findById(userId, function (err, user) {
                        if (err) {
                            callback(err.toString());
                        } else {
                            callback(null, user, msg);
                        }
                    });
                }else {
                    callback('Message for activate not found');
                }
            },
            function (user, msg) {
                if(!user) {
                    callback('Error not found user');
                } else {
                    Message.sendActivatedLink(user, msg, callback)
                }
            }
        ], callback);
    };

    schema.statics.sendActivatedLink = function (user, msg, callback) {
        mailer.sendEmailActivate(user, msg, callback);
    };

    return mongoose.model(path.basename(module.filename, '.js'), schema);
};

function generationActivateLink(userId) {
    return 'http://' + config.get('domain') + '/auth/activate/' + userId.toString();
}

