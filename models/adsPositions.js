var path = require('path');

module.exports = function (mongoose) {
    var schema = new mongoose.Schema({
        teamId: {type: mongoose.Schema.ObjectId, required: true},
        projectId: {type: mongoose.Schema.ObjectId, default: null},
        positionName: {type: String, required: true},
        skills: [{teamId: {type: mongoose.Schema.ObjectId, required: true}, proficiency: {type: String, required: true}}],
        speakSkills: [{speakId: {type: mongoose.Schema.ObjectId, required: true}, proficiency: {type: String, required: true}}],
        description: {type: String, default: ""},
        author: {type: mongoose.Schema.ObjectId, required:true}

    });

    return mongoose.model(path.basename(module.filename, '.js'), schema);
};

