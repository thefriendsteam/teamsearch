var path = require('path');

module.exports = function (mongoose) {
    var schema = new mongoose.Schema({
        name: {type: String, required: true},
        shortName: [{type: String}],
    });

    return mongoose.model(path.basename(module.filename, '.js'), schema);
};

