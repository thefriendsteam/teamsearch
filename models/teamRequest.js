var path = require('path');

module.exports = function (mongoose) {
    var schema = new mongoose.Schema({
        teamId: {type: mongoose.Schema.ObjectId, required: true},
        senderId: {type: mongoose.Schema.ObjectId, required: true},
        recipientId: {type: mongoose.Schema.ObjectId, required: true},
        description: {type: String, default: ""},
        accepted: {type: Boolean, default: false},
        rejected: {type: Boolean, default: false}
    });

    return mongoose.model(path.basename(module.filename, '.js'), schema);
};

