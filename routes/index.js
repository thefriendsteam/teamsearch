var express = require('express');
var router = express.Router();

module.exports.setup = function(handlers, passport){
  //Main
  router
      .get('/', handlers.home)
      .get('/profile', handlers.auth.isLoggedIn, handlers.home.profile)
      .get('/about', handlers.home.about);
  //User
  router
      .get('/users', handlers.users.list)
      .get('/users/:id', handlers.users.get)
      .post('/users', handlers.users.create)
      .put('/users/:id', handlers.users.update)
      .delete('/users/:id', handlers.users.remove);
  //Auth
  router
      .get('/auth/login', handlers.auth.isNotLogIn, handlers.auth.login)
      .post('/auth/login', handlers.auth.isNotLogIn, passport.authenticate('local-login', {
          successRedirect : '/profile', // redirect to the secure profile section
          failureRedirect : '/auth/login', // redirect back to the signup page if there is an error
          failureFlash : true // allow flash messages
      }))
      .post('/auth/signup', handlers.auth.isNotLogIn, passport.authenticate('local-signup', {
          successRedirect : '/auth/activate', // redirect to the secure profile section
          failureRedirect : '/auth/signup', // redirect back to the signup page if there is an error
          failureFlash : true // allow flash messages
      }))
      .get('/auth/signup', handlers.auth.isNotLogIn, handlers.auth.signup)
      .get('/auth/activate/:id', handlers.auth.activate)
      .get('/auth/activate', handlers.auth.isLoggedIn, handlers.auth.notActive)
      .post('/auth/activate', handlers.auth.isLoggedIn, handlers.auth.resendActivateMsg)
      .get('/logout', handlers.auth.isLoggedIn, handlers.auth.logout);
  return router;
};