(function() {

    var app = {

        initialize : function () {
            this.setUpListeners();
        },

        setUpListeners: function () {
            $('form').on('submit', app.submitForm);
            $('form').on('keydown', '.has-error', app.removeError);
        },

        submitForm: function (e) {

            var form = $(this);
            var submitBtn = form.find('button[type="submit"]');

            if ( app.validateForm(form) === false ) {
                return false;
            } else {

                if (!app.moreValidateForm(form)) {
                    return false;
                } else {
                    submitBtn.attr({disabled: 'disabled'});
                    return;
                }

            }
        },

        validateForm: function (form){

            var inputs = form.find('input');
            var valid = true;

            inputs.tooltip('destroy');

            $.each(inputs, function(index, val) {
                var input = $(val);
                var val = input.val();
                var formGrout = input.parents('.input-group');
                var label = formGrout.parents('.form-group').find('label').text().toLowerCase();
                var textError = 'Enter ' + label;

                if(val.length === 0){
                    formGrout.addClass('has-error').removeClass('has-success');
                    input.tooltip({
                        trigger: 'manual',
                        placement: 'right',
                        title: textError
                    }).tooltip('show');
                    valid = false;
                }else{
                    formGrout.removeClass('has-error').addClass('has-success');
                    input.tooltip('hide');
                }
            });

            return valid;

        },

        removeError: function() {
            $(this).removeClass('has-error').find('input').tooltip('destroy');
        },

        moreValidateForm: function (form) {
            var isValid = true;

            var email = form.find('input[id="email"]');

            if (!app.validateEmail(email.val())){

                var formGrout = email.parents('.input-group');
                formGrout.addClass('has-error').removeClass('has-success');

                email.tooltip({
                    trigger: 'manual',
                    placement: 'right',
                    title: 'Incorrect email'
                }).tooltip('show');

                isValid = false;
            }
            var confirmPasswordField = form.find('input[id="pw2"]');

            if (confirmPasswordField.length) {
                var passwordField = form.find('input[id="pw"]');

                if (confirmPasswordField.val() != passwordField.val()) {
                    var formGrout = confirmPasswordField.parents('.input-group');
                    formGrout.addClass('has-error').removeClass('has-success');

                    confirmPasswordField.tooltip({
                        trigger: 'manual',
                        placement: 'right',
                        title: 'Incorrect password'
                    }).tooltip('show');

                    isValid = false;
                }
            }

            return isValid;

        },

        validateEmail: function (email) {
            var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            return regex.test(email);
        }

    }

    app.initialize();

}());