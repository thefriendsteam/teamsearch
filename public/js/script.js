var modal = document.getElementById('tsg-login-form');

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
};

function showElementById(id){
    document.getElementById(id).style.display = 'block';
}
function hideElementById(id){
    document.getElementById(id).style.display = 'none';
}